## Notebooks

- **cas_resolver_example.ipynb** : Exemple d'utilisation des fonctions de récupération de l'identifiant CAS à partir du nom d'une molécule via scrapping.

- **get_cas_deduct_v2.ipynb** : Récupération du CAS dans DEDuCT

- **pe_database.ipynb** : Croisement et retraitement des données pour créé la base de données des pertubateurs endocriniens.