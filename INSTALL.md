# Installation Guide

## Dependencies

You will need npm to install the dependencies :

```
sudo apt install npm
```

Then, install the project dependencies :

```bash
cd my-app
npm install
```

## Start the application

Build the application, build files will be available in the `dist` folder. :

```bash
npm run build
```

To start the application, run :

```bash
npm run serve
```

By default, the application will be available at http://localhost:4173.