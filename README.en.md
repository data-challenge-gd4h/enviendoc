# Challenge GD4H - EnviEndoc

<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.
 
<a href="https://gitlab.com/data-challenge-gd4h/enviendoc" target="_blank" rel="noreferrer">Website</a> 

## EnviEndoc

Endocrine disruptors (EDs) are chemical substances that can have a negative impact on the human hormonal system by mimicking or blocking the action of natural hormones. Present in numerous products and in the environment, their health impact is a cause for concern.
Data on EPs in the environment is scattered and not very visible. Not all databases are open and, above all, not interoperable.


## **Documentation**

ENVIENDOC is an interactive mapping tool for visualizing environmental data on substances identified as endocrine disruptors, on an international scale.
The website is available at: https://data-challenge-gd4h.gitlab.io/enviendoc/

### **Installation**

[Installation Guide](/INSTALL.md)

### **Contributing**

If you wish to conribute to this project, please follow the [recommendations](/CONTRIBUTING.md).

### **Licence**

The code is published under [MIT Licence](/LICENSE).

The data used are under open license, with the sole condition of citing the resources used.
A table summarizing the data sources used is available on this page:
https://data-challenge-gd4h.gitlab.io/enviendoc/map.html#use-datas

### **Team**

This project was carried out by the EnviEndoc team: https://data-challenge-gd4h.gitlab.io/enviendoc/about.html
