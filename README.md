# Challenge GD4H - EnviEndoc

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.
 
<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a>

## EnviEndoc

Les perturbateurs endocriniens (PE) sont des substances chimiques pouvant avoir un impact négatif sur le système hormonal humain en imitant ou en bloquant l’action des hormones naturelles. Présentes dans de nombreux produits et dans l’environnement, leur impact sanitaire est préoccupant.

Les données sur les PE dans l’environnement sont dispersées et peu visibles. Les bases ne sont pas toutes ouvertes et surtout pas interopérables.

<a href="https://gd4h.ecologie.gouv.fr/defis/787" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

ENVIENDOC est un outil de cartographie interactive permettant de visualiser les données environnementales sur des substances identifiées comme des perturbateurs endocriniens, à l’échelle internationale.

Le site web est disponible à cette adresse : https://data-challenge-gd4h.gitlab.io/enviendoc/

### **Installation**

[Guide d'installation](/INSTALL.md)

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données utilisées sont sous licence ouverte, à la seule condition de citer les ressources utilisées.
Un tableau récapitulant les sources de données utilisées est disponible sur cette page :
https://data-challenge-gd4h.gitlab.io/enviendoc/map.html#use-datas

### **L'équipe**

Ce projet a été réalisé par l'équipe EnviEndoc : https://data-challenge-gd4h.gitlab.io/enviendoc/about.html
